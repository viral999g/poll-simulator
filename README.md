# Enterprise Computing - Lab 1 

**Demo -** [![Heroku](https://heroku-badge.herokuapp.com/?app=poll-simulator-202012027)](http://poll-simulator-202012027.herokuapp.com/)

#### Presentataion Layer - [Frontend Directory](https://gitlab.com/viral999g/poll-simulator/-/tree/master/frontend)
The presentation layer or the UI of this project is made with ReactJS with 5 components in mind:
1. Home Page (VoteScreen.js)
2. Results Page (Results.js)
3. Voting Summary Page (VotingSummary.js)
4. Add Candidate Page (AddCandidate.js)
5. Header Component (Header.js)

#### Business Logic Layer - [Routes Directory](https://gitlab.com/viral999g/poll-simulator/-/tree/master/backend/routes)
This layer manages the routing of the backend APIs and pass it to [controllers](https://gitlab.com/viral999g/poll-simulator/-/tree/master/backend/controllers) for data fetching or manipulation.
1. adminRoutes.js contains business login for admins
2. voteRoutes.js contains business login for users

#### Data Layer - [Model Directory](https://gitlab.com/viral999g/poll-simulator/-/tree/master/backend/models)
This layer contains the login for data access.
1. data.js contains logic of accessing data
