import { Route } from 'react-router-dom'
import { Container } from 'react-bootstrap'
import AddCandidate from './components/AddCandidate';
import Results from './components/Results';
import VoteForm from './components/VoteForm';
import VotingSummary from './components/VotingSummary'
import Header from './components/Header';

function App() {
  return (
    <>
      <Header></Header>
      <Container className='p-3'>
        <Route path='/addCandidate' component={AddCandidate}></Route>
        <Route path='/summary' component={VotingSummary}></Route>
        <Route path='/vote' component={VoteForm}></Route>
        <Route path='/results' component={Results}></Route>
        <Route path='/' component={VoteForm} exact></Route>
      </Container>
    </>
  );
}

export default App;
