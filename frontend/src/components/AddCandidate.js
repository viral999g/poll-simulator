import React, { useState } from 'react'
import { Alert, Button, Col, Form, Row } from 'react-bootstrap'
import axios from 'axios'

const AddCandidate = () => {
  const [candidateId, setCandidateId] = useState('')
  const [candidateName, setCandidateName] = useState('')
  const [status, setStatus] = useState('')


  const submitHandler = async (e) => {
    e.preventDefault()
    if (candidateId === '' || setCandidateId === '') {
      setStatus('Enter valid details')
      return false
    }

    const { data } = await axios.post('/api/admin', {
      candidate_name: candidateName,
      candidate_id: candidateId
    })

    setStatus(data.message)
  }

  return (
    <>
      <h2 className='mb-4 text-secondary'>Add Candidate</h2>
      <Form onSubmit={submitHandler} className='d-flex flex-column'>

        <Form.Group as={Row} className="mb-3" controlId="formHorizontalID">
          <Form.Label column sm={2} style={{ fontSize: '1.3rem' }}>
            Student ID
          </Form.Label>
          <Col sm={4}>
            <Form.Control type="text" placeholder="Student ID" required onChange={(e) => { setStatus(''); setCandidateId(e.target.value) }} minLength={8} />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="mb-3" controlId="formHorizontalName">
          <Form.Label column sm={2} style={{ fontSize: '1.3rem' }}>
            Student Name
          </Form.Label>
          <Col sm={4}>
            <Form.Control type="text" placeholder="Student Name" required onChange={(e) => { setStatus(''); setCandidateName(e.target.value) }} minLength={3} />
          </Col>
          {status !== '' && <Alert className='mt-3' variant='success'>
            {status}
          </Alert >}
        </Form.Group>


        <Form.Group as={Row} className="mb-3">
          <Col sm={2}>
            <Button className='w-100' type="submit">Submit</Button>
          </Col>
        </Form.Group>

      </Form>
    </>
  )
}

export default AddCandidate
