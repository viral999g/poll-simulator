import React, { useState, useEffect } from 'react'
import { Col, Row } from 'react-bootstrap'
import axios from 'axios'

const Results = () => {
  const [results, setResults] = useState([])

  useEffect(() => {
    const getResults = async () => {
      const { data } = await axios.get('/api/results')
      setResults(data)
    }

    getResults()
  }, [])

  return (
    <Row>
      {results.length > 1 && (<Row className='d-flex justify-content-center'>
        <Col sm={12} className='text-center mb-3'>
          <h3 className='text-primary'>Results</h3>
        </Col>
        <Col sm={3} className='border p-3 d-flex flex-column justify-content-center align-items-center results-card shadow'>
          <h4>Winner</h4>
          <p>{results[0].candidate_name}</p>
          <span className='bg-primary text-light'>Votes: {results[0].votes}</span>
        </Col>
        <Col sm={3} className='border p-3 d-flex flex-column justify-content-center align-items-center results-card shadow'>
          <h4>Runner Up</h4>
          <p>{results[1].candidate_name}</p>
          <span className='bg-info text-dark'>Votes: {results[1].votes}</span>
        </Col>
      </Row>)}
    </Row>
  )
}

export default Results
