import React, { useEffect, useState } from 'react'
import { Alert, Button, Col, Form, Row } from 'react-bootstrap';
import axios from 'axios';

const VoteForm = () => {
  const [candidates, setCandidates] = useState({})
  const [voterId, setVoterID] = useState('')
  const [radioValue, setRadioValue] = useState('');
  const [radioTouched, setRadioTouched] = useState(false)
  const [status, setStatus] = useState('')

  useEffect(() => {
    const getCandidates = async () => {
      const { data } = await axios.get('/api/')
      setCandidates(data)
    }

    getCandidates()
  }, [])

  const submitHandler = async (e) => {
    e.preventDefault()
    if (radioValue === '') {
      setRadioTouched(true)
      return false
    }

    const { data } = await axios.post('/api/vote', {
      voter_id: voterId,
      candidate_id: radioValue
    })

    setStatus(data.message)

  }

  var candidatesUI = []
  for (var candidate in candidates) {
    candidatesUI.push(
      <Form.Check
        key={candidate}
        type="radio"
        label={candidates[candidate].candidate_name}
        value={candidates[candidate].candidate_id}
        checked={radioValue === candidates[candidate].candidate_id}
        name="formHorizontalRadios"
        id={candidates[candidate].candidate_id}
        onChange={e => setRadioValue(e.currentTarget.value)}
      />
    )
  }

  return (
    <>
      <h2 className='mb-4 text-secondary'>Voting For XYZ</h2>
      <Form onSubmit={submitHandler} className='d-flex flex-column'>
        <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
          <Form.Label column sm={2} style={{ fontSize: '1.3rem' }}>
            Student ID
          </Form.Label>
          <Col sm={4}>
            <Form.Control type="text" placeholder="Your Student ID" required onChange={(e) => { setStatus(''); setVoterID(e.target.value) }} minLength={8} />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="mb-3">
          <Form.Label as="legend" column sm={2} style={{ fontSize: '1.3rem' }}>
            Give Your Vote
          </Form.Label>
          <Col sm={10}>
            {candidatesUI}

            {radioTouched && radioValue === '' && <Alert className='mt-3' variant='danger'>
              Please select a candidate
            </Alert>}

            {status !== '' && <Alert className='mt-3' variant='success'>
              {status}
            </Alert>}
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="mb-3">
          <Col sm={2}></Col>
          <Col sm={2}>
            <Button className='w-100' type="submit">Submit</Button>
          </Col>
        </Form.Group>
      </Form>
    </>
  )
}

export default VoteForm
