import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Alert, Col, ProgressBar, Row } from 'react-bootstrap'

const VotingSummary = () => {
  const [summary, setSummary] = useState({})
  let totalVotes = 0

  useEffect(() => {
    const getResults = async () => {
      const { data } = await axios.get('/api/admin/votes-data')
      setSummary(data)
    }

    getResults()
  }, [])

  const candidatesUI = []
  for (var c in summary) {
    totalVotes += summary[c].votes
  }

  for (var s in summary) {
    candidatesUI.push(
      <Col sm={9} key={s} className='py-2 mt-1'>
        <Row className='d-flex justify-content-center'>
          <Col sm={2}>{summary[s].candidate_name}</Col>
          <Col sm={7}>
            <ProgressBar animated now={summary[s].votes} max={totalVotes} label={`${summary[s].votes} votes`} />
          </Col>
        </Row>
      </Col >
    )
  }

  return (
    <Row className='d-flex flex-column align-items-center'>
      <Col sm={10} className='text-center mb-3'>
        <h3 className='text-secondary'>Voting Summary <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
          {totalVotes}
          <span class="visually-hidden">unread messages</span>
        </span></h3>

      </Col>
      {candidatesUI}
      <Col sm={7}>
        <Alert className='mt-4 text-center' variant='info'>
          Total Votes - <span style={{ fontWeight: 700, fontSize: '1.2rem' }}>{totalVotes}</span>
        </Alert>
      </Col>
    </Row>
  )
}

export default VotingSummary
