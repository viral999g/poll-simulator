import React from 'react'
import { LinkContainer } from 'react-router-bootstrap';
import { Container, Nav, Navbar } from 'react-bootstrap';

const Header = () => {
  return (
    <header>
      <Navbar
        className='py-3 text-white'
        bg='dark'
        variant='dark'
        expand='lg'
        collapseOnSelect>
        <Container>
          <LinkContainer to='/'>
            <Navbar.Brand>Poll Simulator</Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='ms-auto'>
              <LinkContainer to='/vote'>
                <Nav.Link>
                  <i className='fas fa-vote-yea'></i> Vote
                </Nav.Link>
              </LinkContainer>

              <LinkContainer to='/results'>
                <Nav.Link>
                  <i className='fas fa-poll'></i> Results
                </Nav.Link>
              </LinkContainer>

              <LinkContainer to='/summary'>
                <Nav.Link>
                  <i className='fas fa-clipboard'></i> Votes Summary
                </Nav.Link>
              </LinkContainer>

              <LinkContainer to='/addcandidate'>
                <Nav.Link>
                  <i className='fas fa-user-plus'></i> Add Candidate
                </Nav.Link>
              </LinkContainer>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  )
}

export default Header
