let candidates = {}
let voters = []

candidates['202012027'] = { 'candidate_name': "Immanuel Kant", votes: 2 }
candidates['202012111'] = { 'candidate_name': "René Descartes", votes: 4 }
candidates['202012113'] = { 'candidate_name': "Ludwig Wittgenstein", votes: 3 }
candidates['202012089'] = { 'candidate_name': "Baruch Spinoza", votes: 0 }

export const addCandidate = (candidate_id, candidate_name) => {
  candidates[candidate_id] = { candidate_name: candidate_name, votes: 0 }
  console.log(candidates[candidate_id])
  return candidates[candidate_id];
}

export const getCandidates = () => {
  var candidatesNames = []
  for (var key in candidates) {
    candidatesNames.push({ candidate_id: key, candidate_name: candidates[key].candidate_name })
  }
  return { ...candidatesNames }
}

export const getCandidatesWVotes = () => {
  return { ...candidates }
}

export const findCandidate = (candidate_id) => {
  if (candidate_id in candidates) {
    return candidates[candidate_id]
  } else {
    return null
  }
}

export const findVoter = (voter_id) => {
  return voters.find(ele => ele === voter_id)
}

export const addVote = (voter_id, candidate_id) => {
  candidates[candidate_id].votes += 1
  voters.push(voter_id)
}

export const findTopTwo = () => {
  var winner = { candidate_id: undefined, candidate_name: undefined, votes: -1 }
  var runnerUp = { candidate_id: undefined, candidate_name: undefined, votes: -1 }

  for (var can in candidates) {
    if (winner.votes < candidates[can].votes) {
      if (can !== winner.candidate_id) {
        runnerUp.candidate_id = winner.candidate_id
        runnerUp.candidate_name = winner.candidate_name
        runnerUp.votes = winner.votes
      }
      winner.candidate_id = can
      winner.candidate_name = candidates[can].candidate_name
      winner.votes = candidates[can].votes

    }

    if (runnerUp.votes < candidates[can].votes && candidates[can].votes <= winner.votes && can !== winner.candidate_id) {
      runnerUp.candidate_id = candidates[can].candidate_id
      runnerUp.candidate_name = candidates[can].candidate_name
      runnerUp.votes = candidates[can].votes
    }
  }

  return [winner, runnerUp]
}