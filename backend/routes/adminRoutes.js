import express from "express";
const router = express.Router();

import { addNewCandidate, getVotesData } from '../controllers/adminController.js'

router.route('/').post(addNewCandidate)
router.route('/votes-data').get(getVotesData)

export default router