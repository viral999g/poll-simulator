import express from "express";
const router = express.Router();

import { getCandidatesData, vote, getTopTwo } from '../controllers/voteControllers.js'

router.route('/').get(getCandidatesData)
router.route('/vote').post(vote)
router.route('/results').get(getTopTwo)

export default router