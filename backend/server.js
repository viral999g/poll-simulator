import path from 'path';
import express from 'express';
import voteRoutes from './routes/voteRoutes.js'
import adminRoutes from './routes/adminRoutes.js'

const app = express();
const PORT = process.env.PORT || 5000;
const mode = 'production'

app.use(express.json());

app.use('/api/', voteRoutes)
app.use('/api/admin', adminRoutes)

const __dirname = path.resolve();
if (mode === 'production') {
  app.use(express.static(path.join(__dirname, '/frontend/build')));
  app.get('*', (req, res) =>
    res.sendFile(path.resolve(__dirname, 'frontend', 'build', 'index.html'))
  );
} else {
  app.get('/', (req, res) => {
    res.send('API is running...');
  });
}

app.listen(PORT, () =>
  console.log(
    `Server running in development mode at port ${PORT}`
  )
);