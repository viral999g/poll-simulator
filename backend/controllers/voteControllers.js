import { findTopTwo, addVote, findCandidate, findVoter, getCandidates } from "../models/data.js";

export const getCandidatesData = (req, res, next) => {
  res.json(getCandidates())
}

export const vote = (req, res, next) => {
  try {
    const voter_id = req.body.voter_id
    const candidate_id = req.body.candidate_id

    if (findVoter(voter_id)) {
      res.send({ message: 'You have already voted!' })
      return false
    }

    if (findCandidate(candidate_id)) {
      addVote(voter_id, candidate_id)
    } else {
      throw new Error('Invalid Candidate')
    }

    res.send({ message: 'Your vote has been successfully registered' })

  } catch (e) {
    res.status(400).json({ message: e.message })
  }
}

export const getTopTwo = (req, res, next) => {
  res.json(findTopTwo())
}