import { addCandidate, addVote, findCandidate, findVoter, getCandidates, getCandidatesWVotes } from "../models/data.js";

export const addNewCandidate = (req, res, next) => {
  try {
    const candidate_id = req.body.candidate_id
    const candidate_name = req.body.candidate_name

    const checkIfExists = findCandidate(candidate_id)
    if (checkIfExists) {
      res.status(200).json({ message: "Candidate with that ID already exists" })
    } else {
      const newCandidate = addCandidate(candidate_id, candidate_name)
      console.log(newCandidate)
      res.status(201).json({ message: "Successfully Added", candidate: newCandidate })
    }
  } catch (e) {
    res.status(400).json({ message: e.message })
  }
}

export const getVotesData = (req, res, next) => {
  res.json(getCandidatesWVotes())
}